<p align="center">
    <img src="logo.svg" alt="logo"/>
</p>
<p align="center">
    &nbsp;<img src="https://img.shields.io/packagist/v/agrodata/keycloak-guard.svg" />
     <img src="https://img.shields.io/packagist/dt/agrodata/keycloak-guard.svg" />
</p>

## Keycloak Guard
Biblioteca responsável por realizar a autenticação e permissionamento do keycloak nos projetos laravel.
Para a autenticação foi utilizado a biblioteca do Robson Tenorio ([keycloak-guard](https://github.com/robsontenorio/laravel-keycloak-guard)),
para autorização (permissionamento) foi feita uma extensão dessa biblioteca, onde é feita uma busca do
permissionamento do usuário autenticado no keycloak.

### Instalação
Instale o pacote através do composer com o seguinte comando:

```php
composer require agrodata/keycloak-guard
```

Na sequencia adicione a seguinte linha no arquivo ```config/app.php```
```php
<?php #config/app.php

'providers' => [
    ...
    Agrodata\Keycloak\KeycloakGuardServiceProvider::class
]
```

No arquivo ```config/auth.php``` adicione o guard ```agrodata-keycloak```:
```php
<?php #config/auth.php
...
'guards' => [
    'api' => [
        'driver' => 'agrodata-keycloak',
        'provider' => 'users'
    ]
    ...
],
```

### Configuração
Para entender como configurar o permissionamento no keycloak, consultar o seguinte artigo:
https://www.appsdeveloperblog.com/fine-grained-authorization-services-in-keycloak/

#### Configuração da autenticação
Para configurar a autenticação deverá utilizar a documentação da biblioteca pai: https://github.com/robsontenorio/laravel-keycloak-guard#configuration

#### Configuração da autorização

O configuração do permissionamento ficará em um arquivo de configuração separado, pois, é uma funcionalidade
opcional.

```php
<?php # config/agrodata-keycloak.php

return [
    'enabled' => env('AGRODATA_KC_ENABLED', true),
    'endpoint' => env('AGRODATA_KC_ENDPOINT'),
    'client-id' => env('AGRODATA_KC_CLIENT_ID'),
    'realm' => env('AGRODATA_KC_REALM'),
    'concat-resource-scope' => env('AGRODATA_KC_CONCAT_RESOURCE_SCOPE', true),
];
```
<br>

- ```AGRODATA_KC_ENABLED```: informa se a funcionalidade de permissionamento será usada<br>
- ```AGRODATA_KC_ENDPOINT```: UR bLase do keycloak<br>
- ```AGRODATA_KC_CLIENT_ID```: Id do cliente criado dentro do keycloak, onde foi configurado o permissionamento (no keycloak aba "Authorization")<br>
- ```AGRODATA_KC_CLIENT_SECRET```: Secret do client. Utilizado apenas quando é necessário acessar as rotas da api do Keycloak através do Projeto atual<br>
- ```AGRODATA_KC_REALM```: Nome exato do realm criado dentro do keycloak<br>
- ```AGRODATA_KC_CONCAT_RESOURCE_SCOPE```: Concatena o nome do "resource" e "scopes" criado no keycloak. Ex.: resource: ```book```, scopes ```read```, ```write```
. Resultará em string de permissões: ```book.read```, ```book.write```<br>

### Recursos/Serviços
#### Proteção de Recursos
Após a configuração da biblioteca, as permissões do usuários serão registrados no ```Gate``` do Laravel, portanto, será possível
proteger os recursos da api com tais permissões. Um exemplo protegendo as rotas:

```php
<?php #routes\api.php
    Route::middleware('can:book.read')->get('/book/list');
    Route::middleware('can:book.write')->post('/book/store', 'Gravação do recurso');
```
#### Serviço
A biblioteca utiliza a seguinte classe de serviço para realizar a busca das permissões e recursos no keycloak após a autenticação.
Porém, caso deseje utilizar o serviço em qualquer outro lugar do sistema não há problemas. Exemplos:
```php
<?php

/**
* Autenticação | Buscar Bearer token do usuário
*/
$bearerToken = (new \Agrodata\Keycloak\KeycloakAuthService($username, $passwrd))->auth($clientSecret)


/**
* Instanciar Serviço
 * 
 * Recebe token por parametro. Caso nulo, busca em cache o token do usuário autenticddo
*/
$kcService = new Agrodata\Keycloak\KeycloakService($bearerToken = null);

/**
* Buscar permissões 
*/
$permissions = $kcService->getOpenIdPermissions(); #busca permissões do token informado na api do keycloak
ou
$permissions = array_keys(Gate::abilities()) #busca permissões de cache registrados após autenticação


/**
*  Criar subgrupo no keycloak
*/
$kcService->createSubGroup('Novo subgrupo 1', 'Grupo A'); 
```
