<?php

namespace Agrodata\Keycloak;

use Cache;
use Exception;
use Http;

class KeycloakApiService extends KeycloakService
{
    private string|null $token;

    public function __construct(string|null $accessToken = null)
    {
        parent::__construct();
        $this->token = $accessToken ?: request()->bearerToken();
    }

    public function getOpenIdPermissions(): array
    {
        $result = Http::withToken($this->token)
            ->asForm()
            ->post("$this->url/realms/$this->realm/protocol/openid-connect/token", [
                'grant_type' => 'urn:ietf:params:oauth:grant-type:uma-ticket',
                'audience' => $this->clientId,
                'response_mode' => 'permissions'
            ]);

        if ($result->status() === 200) {
            return $result->json();
        }
        return [];
    }

    public function listGroups(string $search = ""): array
    {
        $request = Http::withToken($this->token)->get("$this->url/admin/realms/$this->realm/groups", [
            "search" => $search,
        ]);

        if ($request->status() === 200) {
            return $request->json();
        }
        return [];
    }

    public function createGroup(string $name): int
    {
        return Http::withToken($this->token)
            ->post("$this->url/admin/realms/$this->realm/groups", [
                "name" => $name,
                "path" => $name,
            ])
            ->status();
    }

    public function createSubGroup(string $name, string $groupId): int
    {
        return Http::withToken($this->token)
            ->post("$this->url/admin/realms/$this->realm/groups/$groupId/children", [
                "name" => $name,
                "path" => $name,
            ])
            ->status();
    }

    /**
     * @throws Exception
     */
    public function getClient(): array
    {
        $request = Http::withToken($this->token)->get("$this->url/admin/realms/$this->realm/clients", [
            "clientId" => $this->clientId,
            "first" => 0,
            "max" => 1,
            "search" => true
        ]);

        if ($request->status() === 200) {
            $result = $request->json();
            return array_shift($result);
        }
        if ($request->status() === 403) {
            throw new Exception('[403] User Forbidden to View Realm Clients');
        }
        return [];
    }

    /**
     * @throws Exception
     */
    public function getClientUuid(): string
    {
        if (!empty($client = $this->getClient())) {
            return $client['id'];
        }
        throw new Exception('client not founded');
    }

    /**
     * @throws Exception
     */
    public function listAuthorizationScopes(string $search = "")
    {
        $request = Http::withToken($this->token)->get(
            "$this->url/admin/realms/$this->realm/clients/{$this->getClientUuid()}/authz/resource-server/scope",
            [
                "deep" => false,
                "first" => 0,
                //"max" => 20,
                "name" => $search
            ]
        );

        if ($request->status() === 200) {
            return $request->json();
        }
        return [];
    }

    public function getAuthorizationScope(string $scopeName): array
    {
        $request = Http::withToken($this->token)->get(
            "$this->url/admin/realms/$this->realm/clients/$this->clientId/authz/resource-server/scope",
            [
                "deep" => false,
                "first" => 0,
                "max" => 1,
                "name" => $scopeName
            ]
        );

        if ($request->status() === 200) {
            $result = $request->json();
            return array_shift($result);
        }
        return [];
    }

    /**
     * @throws Exception
     */
    public function getAuthorizationScopeUuid(string $clientId, string $scopeName): string
    {
        if (!empty($scope = $this->getAuthorizationScope($scopeName))) {
            return $scope['id'];
        }
        throw new Exception("Scope [$scopeName] not founded");
    }

    /**
     * @throws Exception
     */
    public function createResource(string $name, string $type, array $scopes = []): int
    {
        $clientUuid = $this->getClientUuid();
        return Http::withToken($this->token)
            ->post("$this->url/admin/realms/$this->realm/clients/$clientUuid/authz/resource-server/resource", [
                "name" => $name,
                "displayName" => $name,
                "type" => $type,
                "scopes" => $scopes
            ])
            ->status();
    }

    /**
     * @throws Exception
     */
    public function retrieveScopesByNames(array $scopesName): array
    {
        $this->validateScopes($scopesName);
        $authorizationScopes = Cache::remember("kc-scopes-".config('app.env'), 60 * 15, function () {
            return $this->listAuthorizationScopes();
        });

        return array_values(array_filter($authorizationScopes, fn ($scope) => in_array($scope['name'], $scopesName)));
    }

    /**
     * @throws Exception
     */
    public function validateScopes(array $scopes): bool
    {
        foreach ($scopes as $scope) {
            if (!$this->isValidScope($scope)) {
                throw new Exception(
                    "Scope [$scope] is unacceptable value. Acceptable [read, write, update, delete]"
                );
            }
        }
        return true;
    }

    public function isValidScope(string $scope): bool
    {
        return in_array($scope, ['read', 'write', 'update', 'delete']);
    }
}
