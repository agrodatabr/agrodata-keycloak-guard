<?php

namespace Agrodata\Keycloak;

use Http;

class KeycloakAuthService extends KeycloakService
{
    private string $username;
    private string $password;

    public function __construct(string $username, string $password)
    {
        parent::__construct();
        $this->username = $username;
        $this->password = $password;
    }

    public function auth(string|null $clientSecret = null): string|null
    {
        $result = Http::asForm()
            ->post("$this->url/realms/basf/protocol/openid-connect/token", [
                'client_id' => $this->clientId,
                'client_secret' => $clientSecret ?? $this->clientSecret,
                'username' => $this->username,
                'password' => $this->password,
                'grant_type' => 'password'
            ]);

        if ($result->status() === 200 && $user = $result->json()) {
            return $user['access_token'];
        }
        return null;
    }
}
