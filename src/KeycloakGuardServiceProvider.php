<?php

namespace Agrodata\Keycloak;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;

class KeycloakGuardServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([__DIR__.'/../config/agrodata-keycloak.php' => config_path('agrodata-keycloak.php')], 'config');
        $this->mergeConfigFrom(__DIR__.'/../config/agrodata-keycloak.php', 'agrodata-keycloak');
    }

     public function register()
     {
         Auth::extend('agrodata-keycloak', function ($app, $name, array $config) {
             return new KeycloakGuard(Auth::createUserProvider($config['provider']), $app->request);
         });
     }
}
