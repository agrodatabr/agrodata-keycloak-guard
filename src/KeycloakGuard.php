<?php

namespace Agrodata\Keycloak;

use Exception;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use KeycloakGuard\KeycloakGuard as RobsonTenorioKeycloakGuard;

class KeycloakGuard extends RobsonTenorioKeycloakGuard
{
    public function __construct(UserProvider $provider, Request $request)
    {
        parent::__construct($provider, $request);
    }

    public function authenticate()
    {
        return parent::authenticate();
    }

    public function validate(array $credentials = []): void
    {
        try {
            if (parent::validate($credentials) && config('agrodata-keycloak.enabled')) {
                $decodedToken = json_decode($this->token());
                $encodedToken = $this->getTokenForRequest();

                if (isset($decodedToken->authorization->permissions)) {
                    $this->registerPermissionsGate($decodedToken->authorization->permissions);
                }
                $this->registerPermissionsGate((new KeycloakApiService($encodedToken))->getOpenIdPermissions());
            }
        } catch (Exception $exception) {
            Log::error(json_encode($exception));
        }
    }

    private function registerPermissionsGate(array $permissions): void
    {
        foreach ($permissions as $item) {
            if (isset($item['scopes'])) {
                foreach ($item['scopes'] as $scope) {
                    if (config('agrodata-keycloak.concat-resource-scope')) {
                        $separator = config('agrodata-keycloak.concat-resource-scope-separator', ".");
                        $scope = "{$item['rsname']}{$separator}{$scope}";
                    }
                    Gate::define(trim($scope), fn () => true);
                }
            }
        }
    }

    public function groups(): array
    {
        return json_decode($this->token())->groups ?? [];
    }

    public function roles(): array
    {
	return json_decode($this->token())->resource_access?->{config('agrodata-keycloak.client-id')}?->roles ?? [];
    }
}
