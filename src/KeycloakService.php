<?php

namespace Agrodata\Keycloak;

use Exception;

class KeycloakService
{
    protected string $url;
    protected string $clientId;
    protected string $realm;
    protected string|null $clientSecret;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        if (!$endpoint = config('agrodata-keycloak.endpoint')) {
            throw new Exception('[endpoint] config param not founded');
        }
        if (!$realm = config('agrodata-keycloak.realm')) {
            throw new Exception('[realm] config param not founded');
        }
        if (!$clientId = config('agrodata-keycloak.client-id')) {
            throw new Exception('[client_id] config param not founded');
        }

        $this->url = $endpoint;
        $this->clientId = $clientId;
        $this->realm = $realm;
        $this->clientSecret = config('agrodata-keycloak.client-secret', null);
    }
}
