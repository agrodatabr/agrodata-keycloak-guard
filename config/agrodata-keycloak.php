<?php

return [
    'enabled' => env('AGRODATA_KC_ENABLED', true),
    'endpoint' => env('AGRODATA_KC_ENDPOINT'),
    'client-id' => env('AGRODATA_KC_CLIENT_ID'),
    'client-secret' => env('AGRODATA_KC_CLIENT_SECRET'),
    'realm' => env('AGRODATA_KC_REALM'),
    'concat-resource-scope' => env('AGRODATA_KC_CONCAT_RESOURCE_SCOPE', true),
    'concat-resource-scope-separator' => env('AGRODATA_KC_CONCAT_RESOURCE_SCOPE_SEPARATOR', ":"),
];
